import React from "react";
import "./App.css";
import { BarChartC } from "./components/barChartC";
import { BarChartR } from "./components/barChartR";
import { DonutChartC } from "./components/donutChartC";
import { DonutChartR } from "./components/donutChartR";
import { LineGraphC } from "./components/lineGraphC";
import { LineGraphR } from "./components/lineGraphR";

function App() {
  return (
    <div className="App">
      <h1>LineGraph</h1>
      <div>
        <span>chart.js</span>
        <LineGraphC />
      </div>
      <br />
      <br />
      <br />
      <div>
        <span>rechart</span>
        <LineGraphR />
      </div>
      <h1>BarGraph</h1>
      <div>
        <span>chart.js</span>
        <BarChartC />
      </div>
      <br />
      <br />
      <br />
      <div>
        <span>rechart</span>
        <BarChartR />
      </div>
      <h1>Donut Chart</h1>
      <div>
        <span>chart.js</span>
        <DonutChartC />
      </div>
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
      <div>
        <span>rechart</span>
        <DonutChartR />
      </div>
    </div>
  );
}

export default App;
