import React from "react";
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  Legend,
} from "chart.js";
import { Bar } from "react-chartjs-2";
import styled from "styled-components";

ChartJS.register(
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  Legend
);

export const options = {
  indexAxis: "y",
  elements: {
    bar: {
      borderWidth: 1,
    },
  },
  responsive: true,
  plugins: {
    title: {
      display: false,
      text: "Chart.js Horizontal Bar Chart",
    },
  },
};

const labels = ["A", "B"];

export const data = {
  labels,
  datasets: [
    {
      label: "Data",
      data: [4000, 3000],
      backgroundColor: "#8884d8",
    },
  ],
};

export const BarChartC = () => {
  return (
    <Container>
      {" "}
      <Bar options={options} data={data} />{" "}
    </Container>
  );
};

const Container = styled.div`
  width: 500px;
  height: 200px;
`;
