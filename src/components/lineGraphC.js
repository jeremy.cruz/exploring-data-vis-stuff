import React from "react";
import styled, { css } from "styled-components";
import { Line } from "react-chartjs-2";
import { Chart, registerables } from "chart.js";
Chart.register(...registerables);

export const LineGraphC = () => {
  return (
    <Container>
      <Line
        datasetIdKey="id"
        data={{
          labels: ["Jun", "Jul", "Aug"],
          datasets: [
            {
              id: 1,
              label: "A",
              data: [5, 6, 7],
              backgroundColor: "#FF5733",
              borderColor: "#FF5733",
            },
            {
              id: 2,
              label: "B",
              data: [3, 2, 1],
              backgroundColor: "#0D17E3",
              borderColor: "#0D17E3",
            },
          ],
        }}
      />
    </Container>
  );
};

const Container = styled.div`
  width: 500px;
  height: 200px;
`;
