import React from "react";
import { Chart as ChartJS, ArcElement, Tooltip, Legend } from "chart.js";
import { Doughnut } from "react-chartjs-2";
import styled from "styled-components";

ChartJS.register(ArcElement, Tooltip, Legend);

//https://stackoverflow.com/questions/43118408/how-to-disable-a-tooltip-for-a-specific-dataset-in-chartjs reference to filter hover
export const data = {
  labels: ["Data"],
  datasets: [
    {
      label: "# of Votes",
      data: [75, 25],
      backgroundColor: ["rgba(54, 162, 235, 1)", "rgba(255, 159, 64, 0)"],
      borderColor: ["rgba(54, 162, 235, 1)", "rgba(255, 159, 64, 0)"],
      borderWidth: 1,
    },
  ],
};

export const DonutChartC = () => {
  return (
    <Container>
      {" "}
      <Doughnut data={data} />{" "}
    </Container>
  );
};

const Container = styled.div`
  width: 500px;
  height: 200px;
`;
