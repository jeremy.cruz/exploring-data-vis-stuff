import React from "react";
import { PieChart, Pie } from "recharts";

export const DonutChartR = () => {
  // Sample data
  const data = [
    { name: "Likes", students: 70, fill: "rgba(54, 162, 235, 1)" },
    { name: "Dislikes", students: 30, fill: "rgba(255, 159, 64, 0)" },
  ];

  return (
    <PieChart width={700} height={700}>
      <Pie
        data={data}
        dataKey="students"
        outerRadius={250}
        innerRadius={150}
        fill="fill"
        startAngle="0"
        endAngle="360"
        nameKey="name"
      />
    </PieChart>
  );
};
