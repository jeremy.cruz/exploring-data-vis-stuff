import {
  Bar,
  Legend,
  Text,
  BarChart,
  CartesianGrid,
  XAxis,
  YAxis,
} from "recharts";

const data = [
  { name: "A", data: 4000, fill: "#8884d8" },
  { name: "B", data: 3000, fill: "#82ca9d" },
];

export const BarChartR = () => (
  <BarChart
    width={730}
    height={250}
    data={data}
    layout="vertical"
    barGap={1}
    barSize={20}
  >
    <CartesianGrid strokeDasharray="3 3" />
    <XAxis type="number" />
    <YAxis type="category" dataKey="name" />
    <Text />
    <Bar dataKey="data" fill="fill" width={"20%"} />
  </BarChart>
);
