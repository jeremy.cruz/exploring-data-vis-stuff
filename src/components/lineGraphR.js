import React from "react";
import {
  LineChart,
  Line,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
  ResponsiveContainer,
} from "recharts";

const data = [
  {
    name: "Jun",
    A: 5,
    B: 3,
  },
  {
    name: "Jul",
    A: 6,
    B: 2,
  },
  {
    name: "Aug",
    A: 7,
    B: 1,
  },
];

export const LineGraphR = () => {
  return (
    <LineChart
      width={500}
      height={200}
      data={data}
      margin={{
        top: 5,
        right: 30,
        left: 20,
        bottom: 5,
      }}
    >
      <CartesianGrid strokeDasharray="3 3" />
      <XAxis dataKey="name" />
      <YAxis />
      <Tooltip />
      <Legend />
      <Line
        type="monotone"
        dataKey="A"
        stroke="#FF5733"
        activeDot={{ r: 8 }}
        strokeWidth={3}
      />
      <Line
        type="monotone"
        dataKey="B"
        stroke="#0D17E3"
        activeDot={{ r: 8 }}
        strokeWidth={3}
      />
    </LineChart>
  );
};
